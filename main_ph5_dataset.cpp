#include <iostream>
#include <cmath>
#include <mpi.h>     /* MPI and MPI-IO live here */

using namespace std;

extern "C" {
#include "ph5_file_utils.h"
}

typedef unsigned long long hsize_t;

int
main(int argc, char **argv) {
    s1_t *data;
    hsize_t dimsf[1]; /* dataset dimensions */
    bool compressed = true;

    cout << "You have entered " << argc << " arguments:" << "\n";

    for (int i = 0; i < argc; ++i)
        cout << argv[i] << "\n";
    int expo;
    if (argc == 1) {
        expo = 20;
    } else {
        expo = std::atoi(argv[1]);
    }
    cout << "you choose 2^" << expo << endl;

    /*
     * Initialize MPI
     */
    MPI_Init(&argc, &argv);

    /*
     * Initialize data buffer
     */
    dimsf[0] = pow(2, expo);
    data = (s1_t *) malloc(sizeof(s1_t) * dimsf[0]);
    for (long i = 0; i < dimsf[0]; i++) {
        data[i].a = 1.1d * i;
        data[i].b = 1.1d * i * i;
    }

    if (writeH5compressed(data, dimsf, H5FILE_NAME, compressed) == EXIT_FAILURE) {
        cout << " Error " << endl;
    };


    MPI_Finalize();

    return 0;
}

