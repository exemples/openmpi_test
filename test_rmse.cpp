/*
 * cet exemple ne devrait pas s'appeller rmse
 * il est le résultat d'une question à chatgpt
 * il n'apporte pas grand-chose de plus que ce qui est déjà fait dans le code de Nais
 * àà part la diffrénce directement sur les complexes
 *
 * */

#include <iostream>
#include <vector>
#include <complex>
#include <cmath>

// Function to compute the L2 norm of the error between two arrays of complex numbers
double computeL2Norm(const std::vector<std::complex<double>>& array1, const std::vector<std::complex<double>>& array2) {
    // Ensure both arrays are of the same size
    if (array1.size() != array2.size()) {
        std::cerr << "Error: Arrays must be of the same size!" << std::endl;
        return -1.0;
    }

    double sum = 0.0;
    for (size_t i = 0; i < array1.size(); ++i) {
        std::complex<double> diff = array1[i] - array2[i];
        sum += std::norm(diff); // norm() returns the squared magnitude of the complex number
    }

    return std::sqrt(sum);
}

int main() {
    // Example arrays of complex numbers
    std::vector<std::complex<double>> array1 = {
            {1.0, 2.0},
            {3.0, 4.0},
            {5.0, 6.0}
    };

    std::vector<std::complex<double>> array2 = {
            {1.1, 2.1},
            {2.9, 4.1},
            {4.9, 5.9}
    };

    // Compute the L2 norm of the error between the two arrays
    double l2_norm = computeL2Norm(array1, array2);

    if (l2_norm >= 0.0) {
        std::cout << "L2 norm of the error: " << l2_norm << std::endl;
    }

    return 0;
}
