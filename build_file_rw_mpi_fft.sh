#!/bin/sh

echo "reconstruit le binaire $0 au mesocentre"

mkdir build
cd build
rm -rf *

if [[ -z "${MPI_RUN}" ]]; then
        echo "charger les modules et relancer ce script"
        echo " "
        echo "module load userspace"
        echo "module load cmake/3.29.2"
        echo "module load openmpi/gcc112/psm2/4.0.5"
else
module load userspace
module load cmake/3.29.2
module load openmpi/gcc112/psm2/4.0.5
env CXX=/trinity/shared/apps/tr17.10/x86_64/openmpi-gcc112-psm2-4.0.5/bin/mpicxx cmake -DCMAKE_CXX_FLAGS="-g -DDEBUG -isystem ~/LIBS/include" -DFFTW_ROOT=/home/ceyraud/LIBS ..
make file_rw_mpi_fft VERBOSE=1

fi

