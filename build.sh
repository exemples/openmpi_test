# GH 4 sept 24
# compiler sur nessie sans passer par CLion
# on veut linker avec un hdf5 spécial

mkdir build
cd build
rm -rf *

env CXX=/usr/bin/mpicxx \
 CC=${HOME}/LIBRARY_PARA/LIBRARIES/hdf5-1.12.3/bin/h5pcc \
	      ${HOME}/.local/share/JetBrains/Toolbox/apps/clion-2/bin/cmake/linux/x64/bin/cmake \
       	-DCMAKE_C_FLAGS="-g -DDEBUG -DH5_HAVE_PARALLEL" \
       	-DHDF5_DIR=${HOME}/LIBRARY_PARA/LIBRARIES/hdf5-1.12.3 \
	..

#-DCMAKE_C_COMPILER=/home/me/LIBRARY_PARA/LIBRARIES/hdf5-1.12.3/bin/h5pcc \
#        	-DCMAKE_CXX_COMPILER=mpicxx  \

#h5pcc -c ph5_dataset.c
#mpic++ ph5_dataset.o main_ph5_dataset.cpp


#env CXX=/usr/bin/mpicxx \
#CC=/home/me/LIBRARY_PARA/LIBRARIES/hdf5-1.12.3/bin/h5pcc \
#/home/me/.local/share/JetBrains/Toolbox/apps/clion-2/bin/cmake/linux/x64/bin/cmake \
#-DCMAKE_C_FLAGS="-g -DDEBUG -DH5_HAVE_PARALLEL" \
#-DHDF5_DIR=${HOME}/LIBRARY_PARA/LIBRARIES/hdf5-1.12.3 \
#..

make VERBOSE=1
