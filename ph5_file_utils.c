/*
 *  This example writes data to the HDF5 file.
 *  Number of processes is assumed to be 1 or multiples of 2 (up to 8)
 */

#include "hdf5.h"
#include "stdlib.h"

#include "ph5_file_utils.h"

int
writeH5compressed(s1_t *data, hsize_t *dimsf, char *fichier, bool compressed) {
    /*
     * HDF5 APIs definitions
     */
    hid_t file_id, dset_id;   /* file and dataset identifiers */
    hid_t filespace;          /* file and memory dataspace identifiers */
    /*hsize_t dimsf[] = {NX * NY};*/ /* dataset dimensions */
    /* s1_t *data;*/               /* pointer to data buffer to write */
    hid_t plist_id;           /* property list identifier */
    int i;
    herr_t status;

    hid_t s1_tid;     /* File datatype identifier */
    hsize_t chunk_dim = 0;


    /*
     * MPI variables
     */
    int mpi_size, mpi_rank;
    MPI_Comm comm = MPI_COMM_WORLD;
    MPI_Info info = MPI_INFO_NULL;

    MPI_Comm_size(comm, &mpi_size);
    MPI_Comm_rank(comm, &mpi_rank);

#ifdef DEBUG
    if (mpi_rank == 0) {
      printf("%s:%d dims: %d \n", __FUNCTION__, __LINE__, (int) dimsf[0]);
    }
#endif

    /*
     * Set up file access property list with parallel I/O access
     */
    plist_id = H5Pcreate(H5P_FILE_ACCESS);
    if (plist_id == H5I_INVALID_HID) {
        return EXIT_FAILURE;
    }
    H5Pset_fapl_mpio(plist_id, comm, info);

    /*
     * OPTIONAL: It is generally recommended to set collective
     *           metadata reads on FAPL to perform metadata reads
     *           collectively, which usually allows datasets
     *           to perform better at scale, although it is not
     *           strictly necessary.
     */
    H5Pset_all_coll_metadata_ops(plist_id, true);

    /*
     * OPTIONAL: It is generally recommended to set collective
     *           metadata writes on FAPL to perform metadata writes
     *           collectively, which usually allows datasets
     *           to perform better at scale, although it is not
     *           strictly necessary.
     */
    H5Pset_coll_metadata_write(plist_id, true);

    /*
     * Create a new file collectively and release property list identifier.
     */
    file_id = H5Fcreate(H5FILE_NAME, H5F_ACC_TRUNC, H5P_DEFAULT, plist_id);
    H5Pclose(plist_id);

    /*
     * Create the dataspace for the dataset.
     */
    filespace = H5Screate_simple(RANK, dimsf, NULL);

    /*
     * Create the memory datatype.
     */
    s1_tid = H5Tcreate(H5T_COMPOUND, sizeof(s1_t));
    H5Tinsert(s1_tid,
              "re",
              HOFFSET(s1_t, a
              ), H5T_NATIVE_DOUBLE);
    H5Tinsert(s1_tid,
              "im",
              HOFFSET(s1_t, b
              ), H5T_NATIVE_DOUBLE);


    if (dimsf[0] > 500 && dimsf[0] < 100000) {
        chunk_dim = 256;  /* on prend un multiple de 2 à vérifier sur le FS?! FIXME */
    } else {
        if (dimsf[0] >= 100000) {
            chunk_dim = 1024;  // TODO tune!
        } else {
            chunk_dim = 0;  // no chunk
        }
    }
    //compressed = false;
    if (compressed && chunk_dim > 0) {
        if (mpi_rank == 0) {
            printf("compression %s:%d dims: %d \n", __FUNCTION__, __LINE__, (int) dimsf[0]);
#ifdef DEBUG
            printf("size of chunk: %d \n", (int) chunk_dim);
#endif
        }
        plist_id = H5Pcreate(H5P_DATASET_CREATE);
        /* Dataset must be chunked for compression */
        status = H5Pset_chunk(plist_id, 1, &chunk_dim);

        /* Set ZLIB / DEFLATE Compression using compression level 6.
         * To use SZIP Compression comment out these lines.
         */
        status = H5Pset_deflate(plist_id, 6);

        /* Uncomment these lines to set SZIP Compression
           szip_options_mask = H5_SZIP_NN_OPTION_MASK;
           szip_pixels_per_block = 16;
           status = H5Pset_szip (plist_id, szip_options_mask, szip_pixels_per_block);
        */

        dset_id = H5Dcreate2(file_id, DATASETNAME, s1_tid, filespace, H5P_DEFAULT, plist_id,
                             H5P_DEFAULT);
        H5Pclose(plist_id);
    } else {
        /*
         * Create the dataset with default properties and close filespace.
         */
        dset_id =
                H5Dcreate(file_id, DATASETNAME, s1_tid, filespace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    }

    /*
     * Create property list for collective dataset write.
     */
    plist_id = H5Pcreate(H5P_DATASET_XFER);
    if (plist_id == H5I_INVALID_HID) {
        return EXIT_FAILURE;
    }
    H5Pset_dxpl_mpio(plist_id, H5FD_MPIO_COLLECTIVE);

    /*
     * To write dataset independently use
     *
     * H5Pset_dxpl_mpio(plist_id, H5FD_MPIO_INDEPENDENT);
     */

    status = H5Dwrite(dset_id, s1_tid, H5S_ALL, H5S_ALL, plist_id, data);
    free(data);

    /*
     * Close/release resources.
     */
    H5Dclose(dset_id);
    H5Sclose(filespace);
    H5Pclose(plist_id);
    H5Fclose(file_id);

    if (mpi_rank == 0)
        printf("PHDF5 example finished with no errors\n");

    return 0;
}
