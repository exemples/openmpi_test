# Mise en oeuvre de MPI pour l'accès aux fichiers en parallèle, et pour la FFTW3

## accès aux fichiers
avec le code file_rw_mpi.cpp mis au point par Naïs Altan dans le cadre de la première année de son alternance recherche à l'Institut Fresnel

## calcul de FFT en parallèle avec MPI
c'est le code main_NA.cpp toujours mis au point par Naïs en partant de l'exemple https://www.fftw.org/fftw3_doc/2d-MPI-example.html#g_t2d-MPI-example

en cas d'absence de cmake, compiler simplement avec la ligne:
`
mpicxx -g -Wall -Wextra -Wconversion -Wshadow -Wpedantic main.cpp -lfftw3_mpi
-lfftw3 -lm
`

## Exécution

`mpirun -np 8 ./a.out`

## Exécution avec slurm
pour compiler sur le mesocentre
```
module load userspace
module load gcc/7.2.0
module load cmake/3.29.2
env CC=gcc cmake -DCMAKE_CXX_FLAGS="-g -DDEBUG" ..
```

ou alors, avec un compilo plus récent
```
module load userspace
module load cmake/3.29.2
module load openmpi/gcc112/psm2/4.0.5
env CXX=/trinity/shared/apps/tr17.10/x86_64/openmpi-gcc112-psm2-4.0.5/bin/mpicxx cmake -DCMAKE_CXX_FLAGS="-g -DDEBUG -isystem ~/LIBS/include" -DFFTW_ROOT=/home/ceyraud/L
IBS ..

```

prendre le script suivant, et l'adapter
```
#!/bin/sh
#SBATCH -J fft_mpi_8
#SBATCH -p skylake
#SBATCH -N 8
#SBATCH -t 8:0:0
#SBATCH --mem=180000
#SBATCH -A b214
#SBATCH -o ./run.%N.%j.out
#SBATCH -e ./run.%N.%j.err
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --mail-user=henry@fresnel.fr,nais.atlan@centrale-marseille.fr

#
module load userspace
module load gcc/7.2.0
module load fftw3/gcc72

#
echo “Running on: $SLURM_NODELIST”
echo ""
echo ""
printenv
 
export OMPI_MCA_btl_openib_allow_ib=1
mpirun ./a.out

```
le job est lancé sur 8 noeuds (N=8) 
par exemple, ici, on a diminué la mémoire demandée:
```
[ceyraud@login02 example_1_bis] > diff lance8.slurm lance8_1.slurm 
2c2
< #SBATCH -J fft_mpi_8
---
> #SBATCH -J fft_mpi_8_1
7c7
< #SBATCH --mem=180000
---
> #SBATCH --mem=64000
21c21
< echo ""
---
> echo " test avec 64Go par noeud!!!"

```
mais en perdant du temps FIXME

## Configuration de CLion pour exécuter le code MPI
https://www.jetbrains.com/help/clion/openmpi.html

## Tips
si on exécute le code sur un simple pc, et qu'on a le message:

```
hwloc/linux: Ignoring PCI device with non-16bit domain.
Pass --enable-32bits-pci-domain to configure to support such devices
```

il suffit de faire

`
export HWLOC_HIDE_ERRORS=2
`

# Programme de test xrandpfile
On récupère l'exemple trouvé sur ce site: https://cvw.cac.cornell.edu/parallel-io/mpi-io/exercise.
ce code permet de lire un gros fichier en //.

on modifie le code et on le met dans /home/me/projets/openmpi_test, puis au mesocentre
pour compiler à Fresnel :
```
cd build
cmake -DCMAKE_BUILD_TYPE=Debug ..
make
```

pour compiler au mesocentre:
```
env CC=gcc cmake ..
make
```
et l'exécution juste pour vérifier que tout marche:

```
(base) me@nessie:~/projets/openmpi_test$ mpirun -np 4 ./cmake-build-debug/xrandpfile -f input.txt
input file: input.txt
0: allocated space for filename
0: received broadcast
0: filename = input.txt
1: allocated space for filename
1: received broadcast
1: filename = input.txt
2: allocated space for filename
2: received broadcast
2: filename = input.txt
3: allocated space for filename
3: received broadcast
3: filename = input.txt
2: total_number_of_bytes = 1800
2: allocated 450 bytes
2: my offset = 900
3: total_number_of_bytes = 1800
3: allocated 450 bytes
3: my offset = 1350
0: total_number_of_bytes = 1800
0: allocated 450 bytes
0: my offset = 0
1: total_number_of_bytes = 1800
1: allocated 450 bytes
1: my offset = 450
3: read 450 bytes
3: my offset = 1800
0: read 450 bytes
0: my offset = 450
longest_io_time       = 0.000016 seconds
total_number_of_bytes = 1800
transfer rate         = 104.321712 MB/s
2: read 450 bytes
2: my offset = 1350
1: read 450 bytes
1: my offset = 900

```
pour un fichier input .txt de 1800 caractères, composé de 100 lignes de deux réels.


```
[ceyraud@login01 openmpi_test] > mpirun -np 2 ./build/xrandpfile -f /scratch/ceyraud/Aubis.txt
Not enough memory to read the file.
Consider running on more nodes.
```

Le fichier **xrandpfile.cpp** est transformé en **file_rw_mpi.cpp** pour ajouter l'écriture après une simple modification des données (préparation de la FFT)

On vérifie que le fichier d'entrée et de sortie sont bien identiques.

Et enfin, on part du fichier **file_rw_mpi.cpp** por créer le fichier **file_rw_mpi_fft.cpp** pour ajouter le traitement de la FFT (avec FFTW3)

# les fichiers ph5_dataset.c (on oublie File_create.c et file_create.c)
récupérés sur l'ancien site:
https://docs.hdfgroup.org/archive/support/HDF5/Tutor/pcrtaccf.html
et le nouveau site:
https://docs.hdfgroup.org/hdf5/v1_14/_intro_par_h_d_f5.html
voir le fichier https://github.com/HDFGroup/hdf5/blob/hdf5_1_14/HDF5Examples/C/H5PAR/ph5_dataset.c qui remplace File_create.c
Ce code permet d'écrire une suite d'entiers dans un fichier, le nombre de procs pouvant varier de 1 à 8 ??? FIXME

par contre, c'est un tableau 2D qui est écrit, on a le choix:
- soit de faire Nx=... et NY=1
- soit de n'avoir qu'une dimension
On choisit de n'avoir qu'une dimension, le code est assez rapidement modifié ph5_ds_int1d.c
- on part du fichier original ph5_dataset
- on le modifie pour n'avoir qu'un tableau 1d: ph5_ds_int1d
- on le modifie pour remplacer les entiers par 2 doubles représentant la partie réelle et imaginaire d'un complexe: ph5_ds_comp.c
- une fis que c'est validé, on transforme le fichier en librairie appelable depuis un code C++ 

## tester
```
/home/me/LIBRARY_PARA/LIBRARIES/hdf5-1.12.3/bin/h5pcc ../ph5_dataset.c -o ph5_dataset
mpirun -np 4 ./ph5_dataset
```
et ça donne:
```
me@nessie:~/projets/openmpi_test/build$ h5dump SDS.h5 
HDF5 "SDS.h5" {
GROUP "/" {
   DATASET "IntArray" {
      DATATYPE  H5T_STD_I32LE
      DATASPACE  SIMPLE { ( 40 ) / ( 40 ) }
      DATA {
      (0): 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
      (19): 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34,
      (35): 35, 36, 37, 38, 39
      }
   }
}
}

```
 on modifie le code pour écrire le type double (partie réelle et imaginaire) voir la version git: 80c98c49,
 en s'inspirant de l'example [h5_compound.c](https://github.com/HDFGroup/hdf5/blob/hdf5_1_14/HDF5Examples/C/TUTR/h5_compound.c)
 
## modification de ph5_ds_comp.c en librairie C
- on crée le fichier ph5_file_utils.c, ph5_file_utils.c et on compile avec ph5_ds_comp.c modifié
- maintenant, on essaie d'appeler la lib ph5_file_utils depuis un code C++
- 
et au mesocentre
```
module load userspace
module load cmake/3.29.2
module load openmpi/gcc112/psm2/4.0.5
module load hdf5/gcc72/openmpi/1.10.1

env CC=h5pcc cmake -DCMAKE_C_FLAGS="-g -DDEBUG -DH5_HAVE_PARALLEL" .. 
make ph5_dataset VERBOSE=1
```

# mise en place d ela compression dans ph5_file_utils
avec un code C uniquement: ph5_ds_comp_main.c

avec un code C++: main_ph5_dataset.cpp

Ecriture d'un fichier de 2^28 complexes = 268435456 (soit 268 millions de complexes) ~0.2 Go
les complexes sont sur 64 bits (8 octets) donc environ 4 Go (0,2 * 16)
4,1G -rw-r----- 1 ceyraud ceyraud 4,1G 11 sept. 18:11 /scratch/ceyraud/SDScomp1d.h5
mais je me suis trompé, j'ai oublié de mettre la comression
par contre, ça a mis 1h20 alors qu'on est sur unfilesystem // et qu'on a pris 4 noeuds pour exécuter ce programme

FIXME: il faudrait tester avec un bête programme sans MPI
et surtout pensez à ajouter la compression!!!
avec le code testbigfft.exe de simujura, on avait fait des tests avec 2^30, on est monté à 2^35 qui correspond à 34Go~ voir /home2/henry/fichier2_3x sur athena-4to

par exemple:
henry@athena-4to:~$ head /home2/henry/fichier2_33
1.000000 1e-16
1.000000 1e-16
et ce fichier contient 8589934592 lignes (soit 2^33)
et représente 120Go de taille sur le disque alors que  2^33 ~8,5Go *16 ~= 130Go

ce qui marche:
2^10 job 8753082                                         
2^20 job 8753098
