//
// Created by me on 05/08/24.
//
#include <cmath>
#include <filesystem>
#include <fstream>
#include <iostream>
namespace fs = std::filesystem;

struct HumanReadable
{
    std::uintmax_t size{};

private:
    friend std::ostream& operator<<(std::ostream& os, HumanReadable hr)
    {
        int o{};
        double mantissa = hr.size;
        for (; mantissa >= 1024.; mantissa /= 1024., ++o);
        os << std::ceil(mantissa * 10.) / 10. << "BKMGTPE"[o];
        return o ? os << "B (" << hr.size << ')' : os;
    }
};

int main(int, char const* argv[]) {

   int  puiss = 38;
    size_t total_number_of_bytes = pow(2, puiss); // Size of the array

    std::cout << total_number_of_bytes << " size = " << HumanReadable{total_number_of_bytes} << '\n';
}