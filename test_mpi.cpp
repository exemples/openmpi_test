﻿/*
 * GH 2024/08/01 on calcule FFT et FFT inv directement, sans passer par des fichiers
 * on s'assure que le tableau est bien distribué sur tous les processes MPI
 * FIXME: attention il faut que le nombre d'éléments du tableau soit un multiple de 2
 * ainsi que le nombre de processus MPI
 *
 * %Id: xrandpfile.c,v 1.4 2003/10/18 21:43:05 gustav Exp %
 *
 * %Log: xrandpfile.c,v %
 * Revision 1.4  2003/10/18 21:43:05  gustav
 * integers -> bytes
 *
 * Revision 1.3  2003/10/18 21:33:35  gustav
 * Indented the program (used emacs).
 *
 * Revision 1.2  2003/10/18 20:43:24  gustav
 * Added reading of status with MPI_Get_count
 *
 * Revision 1.1  2003/10/18 19:52:44  gustav
 * Initial revision
 *
 *
 */

#include <cstdio>   /* all IO stuff lives here */
#include <cstdlib>  /* exit lives here */
#include <cstring>  /* strcpy lives here */
#include <mpi.h>     /* MPI and MPI-IO live here */
#include <iostream>
#include <cmath>

using namespace std;

#define CONTROLLER_RANK 0


int main(int argc, char **argv) {
    /* my variables */

    int my_rank, pool_size;
    bool i_am_the_controller = false;

    int puiss;  // puissance à lire sur l'entrée standard

    /* ACTION */

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &pool_size);
    if (my_rank == CONTROLLER_RANK) i_am_the_controller = true;

    // Define the dimensions of the FFT (e.g., 1D FFT)
    ptrdiff_t total_number_of_bytes;
    if (i_am_the_controller) {
        cin >> puiss;
        total_number_of_bytes = pow(2, puiss); // Size of the array
        cout << "taille de  la FFT " << total_number_of_bytes << endl;
    }
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Bcast(&total_number_of_bytes, 1, MPI_INT, CONTROLLER_RANK, MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);
    cout << my_rank << " taille de  la FFT " << total_number_of_bytes << endl;

    MPI_Finalize();
    exit(0);
}
