﻿/*
 * GH 2024/08/01 on calcule FFT et FFT inv directement, sans passer par des fichiers
 * on s'assure que le tableau est bien distribué sur tous les processes MPI
 * FIXME: attention il faut que le nombre d'éléments du tableau soit un multiple de 2
 * ainsi que le nombre de processus MPI
 *
 * %Id: xrandpfile.c,v 1.4 2003/10/18 21:43:05 gustav Exp %
 *
 * %Log: xrandpfile.c,v %
 * Revision 1.4  2003/10/18 21:43:05  gustav
 * integers -> bytes
 *
 * Revision 1.3  2003/10/18 21:33:35  gustav
 * Indented the program (used emacs).
 *
 * Revision 1.2  2003/10/18 20:43:24  gustav
 * Added reading of status with MPI_Get_count
 *
 * Revision 1.1  2003/10/18 19:52:44  gustav
 * Initial revision
 *
 *
 */

#include <cstdio>   /* all IO stuff lives here */
#include <cstdlib>  /* exit lives here */
#include <cstring>  /* strcpy lives here */
#include <iostream>
#include <complex>
#include <cmath>
#include <fftw3-mpi.h>
#include <fstream>

using namespace std;

#define CONTROLLER_RANK 0
/*
#define TRUE 1
#define FALSE 0
#define BOOLEAN int
*/
#define MBYTE 1048576
#define SYNOPSIS printf ("synopsis: %s -f <file>\n", argv[0])
const double tolerance = 1e-10;

double computeL2Norm(fftw_complex *original, fftw_complex *reconstructed, ptrdiff_t local_n0) {
    double sum = 0.0;

    for (ptrdiff_t i = 0; i < local_n0; ++i) {
        complex<double> diff, array1, array2;
        diff = (original[i][0] - reconstructed[i][0]) + i * (original[i][1] - reconstructed[i][1]);
        sum += std::norm(diff); // norm() returns the squared magnitude of the complex number
    }
    return std::sqrt(sum);

}

struct HumanReadable {
    std::uintmax_t size{};

private:
    friend std::ostream &operator<<(std::ostream &os, HumanReadable hr) {
        int o{};
        double mantissa = hr.size;
        for (; mantissa >= 1024.; mantissa /= 1024., ++o);
        os << std::ceil(mantissa * 10.) / 10. << "BKMGTPE"[o];
        return o ? os << "B (" << hr.size << ')' : os;
    }
};

int main(int argc, char **argv) {
    /* my variables */

    int my_rank, pool_size;
    bool i_am_the_controller = false, input_error = false;
    char *filename = NULL, *read_buffer;
    int filename_length;
    int file_open_error, number_of_bytes;
    /* MPI_Offset is long long */

    MPI_Offset my_offset, my_current_offset, total_number_of_bytes,
            number_of_bytes_ll, max_number_of_bytes_ll;
    MPI_File fh;
    MPI_Status status;

    fftw_plan forward_plan, backward_plan;
    fftw_complex *data, *original_data;
    ptrdiff_t alloc_local, local_n0, local_0_start, local_n1, local_1_start;
    int puiss;  // puissance à lire sur l'entrée standard

    /* ACTION */

    MPI_Init(&argc, &argv);

    // Initialize FFTW for MPI
    fftw_mpi_init();

    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &pool_size);
    if (my_rank == CONTROLLER_RANK) i_am_the_controller = true;

    puiss = 16;
    total_number_of_bytes = pow(2, puiss); // Size of the array
    if (i_am_the_controller) {
        cout << " taille de la FFT " << total_number_of_bytes << " (2^" << puiss << " )" << endl;
        std::cout << total_number_of_bytes << " size = " << HumanReadable{(size_t) total_number_of_bytes} << '\n';
    }
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Bcast(&total_number_of_bytes, 1, MPI_INT, CONTROLLER_RANK, MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);

    alloc_local = fftw_mpi_local_size_1d(total_number_of_bytes, MPI_COMM_WORLD, FFTW_FORWARD, FFTW_ESTIMATE,
                                         &local_n0, &local_0_start, &local_n1, &local_1_start);
    data = fftw_alloc_complex(alloc_local);
    original_data = fftw_alloc_complex(alloc_local);
    /* on crée un tableau de données quelconques pour tester la FFT // */
    for (ptrdiff_t i = 0; i < local_n0; ++i) {
        data[i][0] = my_rank * local_n0 + i; // Real part
        data[i][1] = 0.0; // Imaginary part
        // on stocke le tableau original pour la verif uniquement, sinon, pas besoin
        original_data[i][0] = data[i][0]; // Copy the real part
        original_data[i][1] = data[i][1]; // Copy the imaginary part
    }

    /* Création des plans FFT */
    forward_plan = fftw_mpi_plan_dft_1d(total_number_of_bytes, data, data, MPI_COMM_WORLD,
                                        FFTW_FORWARD, FFTW_ESTIMATE);

    backward_plan = fftw_mpi_plan_dft_1d(total_number_of_bytes, data, data, MPI_COMM_WORLD,
                                         FFTW_BACKWARD, FFTW_ESTIMATE);
    if (i_am_the_controller) {
        cout << "calcul de  la FFT " << endl;
    }
    fftw_execute(forward_plan);

    // on fait la FFT inverse
    if (i_am_the_controller) {
        cout << "calcul de  la FFT inverse" << endl;
    }
    fftw_execute(backward_plan);

    // Normalisation des résultats de l'IFFT 
    for (ptrdiff_t i = 0; i < local_n0; ++i) {
        data[i][0] /= total_number_of_bytes;
        data[i][1] /= total_number_of_bytes;
    }

    // Test si la FFT est correcte 
    double local_correctness = computeL2Norm(original_data, data, local_n0);
    double global_correctness;

    MPI_Reduce(&local_correctness, &global_correctness, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);

    if (i_am_the_controller) {
            cout << "err " << global_correctness << " tol " << tolerance << "." << endl;
    }

    fftw_destroy_plan(forward_plan);
    fftw_destroy_plan(backward_plan);

    MPI_File file;
    MPI_File_open(MPI_COMM_WORLD, "output.dat", MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &file);
    if (i_am_the_controller) {
        cout << "on tente l'écriture dans un fichier" << endl;
        char buf[255] ;
        int len;
        for (ptrdiff_t i = 0; i < local_n0; ++i) {
            sprintf(buf, "%f\t%f\t%f\t%f\n", original_data[i][0], original_data[i][1], data[i][0], data[i][1]);
            //cout << buf << endl;
            len = strlen(buf);
            MPI_File_write(file, buf, len, MPI_CHAR, MPI_STATUS_IGNORE);
        }
        cout << "fin écriture dans un fichier" << endl;
    }
    MPI_File_close(&file);

    // on va stocker les résultats dans un fichier
/*    if (i_am_the_controller) {
        ofstream sortie("sortie.dat", ios::out);
        if (!sortie) {
            cerr << "creatio impossible ficiher de sortie de debug" << endl;
        } else {
            for (ptrdiff_t i = 0; i < total_number_of_bytes; ++i) {
                sortie << original_data[i][0] << " " << original_data[i][1]
                <<  " " << data[i][0] << " " << data[i][1]<< endl;
            }
            sortie.close();
        }
    }*/

    fftw_free(data);
    fftw_free(original_data);


    MPI_Finalize();
    exit(0);
}
