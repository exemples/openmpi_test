/*
 *  This example writes data to the HDF5 file.
 *  Number of processes is assumed to be 1 or multiples of 2 (up to 8)
 */

#include "hdf5.h"
#include "stdlib.h"
#include "math.h"

#include "ph5_file_utils.h"

int
main(int argc, char **argv) {
    s1_t *data;
    hsize_t dimsf[1]; /* dataset dimensions */
    int i;
    bool compressed = true;
    ;

    /*
     * Initialize MPI
     */
    MPI_Init(&argc, &argv);

    /*
     * Initialize data buffer
     */
    dimsf[0] = pow(2, 20);

    data = (s1_t *) malloc(sizeof(s1_t) * dimsf[0]);
    for (i = 0; i < dimsf[0]; i++) {
        data[i].a = 1.1d * i;
        data[i].b = 1.1d * i * i;
    }

    if (writeH5compressed(data, dimsf, H5FILE_NAME, compressed) == EXIT_FAILURE) {
        printf("Error");
    };

    MPI_Finalize();

    return 0;
}
