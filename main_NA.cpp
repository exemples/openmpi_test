#include <fftw3-mpi.h>
#include <cmath>
#include <cstdio>
#include <iostream>

#include <random>

using namespace std;

/* Fonction aléatoire */
void my_function(ptrdiff_t x, fftw_complex value, const ptrdiff_t N) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis_real(0.0, 1.0);
    std::uniform_real_distribution<> dis_imag(0.0, 1.0);

    value[0] = dis_real(gen);
    value[1] = dis_imag(gen);
}

/* Fonction porte */
//void my_function(ptrdiff_t x, fftw_complex value, const ptrdiff_t N) {
//    const double center = N / 2.0;
//    const double width = N / 5.0; // 20% of N
//    if (fabs(x - center) <= width / 2.0) {
//        value[0] = 1.0; // Real part
//    } else {
//        value[0] = 0.0; // Real part
//    }
//    value[1] = 0.0; // Imaginary part
//}

/* Fonction sinus */
//void my_function(ptrdiff_t x, fftw_complex value) {
//    value[0] = sin(2 * M_PI * x / 100.0); // Partie réelle
//    value[1] = 0.0; // Partie imaginaire
//}

/* Fonction pour tester si la fft est correcte */
int test_fft_correctness(fftw_complex *original, fftw_complex *reconstructed, ptrdiff_t N, ptrdiff_t local_n0) {
    double tolerance = 1e-10;
    for (ptrdiff_t i = 0; i < local_n0; ++i) {
        if (fabs(original[i][0] - reconstructed[i][0]) > tolerance ||
            fabs(original[i][1] - reconstructed[i][1]) > tolerance) {
            return 0; // FFT est incorrecte
        }
    }
    return 1; // FFT est correcte
}

int main(int argc, char **argv) {


    const ptrdiff_t N = pow(2,31);
    cout<< "nombre de points de la FFT N = " << N << endl;
    fftw_plan forward_plan, backward_plan;
    fftw_complex *data, *original_data;
    ptrdiff_t alloc_local, local_n0, local_0_start, local_n1, local_1_start, i;
    int rank, size;

    MPI_Init(&argc, &argv);
    fftw_mpi_init();

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    double start_time, end_time, total_time;

    start_time = MPI_Wtime();
    alloc_local = fftw_mpi_local_size_1d(N, MPI_COMM_WORLD, FFTW_FORWARD, FFTW_ESTIMATE,
                                         &local_n0, &local_0_start, &local_n1, &local_1_start);
    data = fftw_alloc_complex(alloc_local);
    original_data = fftw_alloc_complex(alloc_local);

    /* initialisation avec my_function(x) */
    for (i = 0; i < local_n0; ++i) {
        my_function(local_0_start + i, data[i],N);
        original_data[i][0] = data[i][0]; // Copy the real part
        original_data[i][1] = data[i][1]; // Copy the imaginary part
    }

    /* création des 2 plans */
    forward_plan = fftw_mpi_plan_dft_1d(N, data, data, MPI_COMM_WORLD,
                                        FFTW_FORWARD, FFTW_ESTIMATE);
    backward_plan = fftw_mpi_plan_dft_1d(N, data, data, MPI_COMM_WORLD,
                                         FFTW_BACKWARD, FFTW_ESTIMATE);

    /* temps pour la fft*/
    double fft_start_time = MPI_Wtime();
    fftw_execute(forward_plan);
    double fft_end_time = MPI_Wtime();

    /* temps pour la fft inverse */
    double ifft_start_time = MPI_Wtime();
    fftw_execute(backward_plan);
    double ifft_end_time = MPI_Wtime();

    /* normaliser les résultats de l'IFFT */
    for (i = 0; i < local_n0; ++i) {
        data[i][0] /= N;
        data[i][1] /= N;
    }

    /* test si la fft est correcte */
    int local_correctness = test_fft_correctness(original_data, data, N, local_n0);
    int global_correctness;

    MPI_Reduce(&local_correctness, &global_correctness, 1, MPI_INT, MPI_MIN, 0, MPI_COMM_WORLD);

    end_time = MPI_Wtime();
    total_time = end_time - start_time;

    if (rank == 0) {
        if (global_correctness) {
            printf("FFT is correct.\n");
        } else {
            printf("FFT is incorrect.\n");
        }
        printf("Total execution time: %f seconds\n", total_time);
        printf("Forward FFT time: %f seconds\n", fft_end_time - fft_start_time);
        printf("Backward FFT time: %f seconds\n", ifft_end_time - ifft_start_time);
    }

    fftw_destroy_plan(forward_plan);
    fftw_destroy_plan(backward_plan);

    fftw_free(data);
    fftw_free(original_data);

    MPI_Finalize();

    return 0;
}
