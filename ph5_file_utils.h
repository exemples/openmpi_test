#ifndef PH5_FILE_UTILS_H
#define PH5_FILE_UTILS_H

#define H5FILE_NAME "SDScomp1d.h5"
#define DATASETNAME "DoubleArray1dNotComp"
#define NX          64 /* dataset dimensions toujours multiple de 2 dans notre cas */
#define NY          1
#define RANK        1

typedef struct s1_t {
    double a;
    double b;
} s1_t;

#include <stdbool.h>
typedef unsigned long long hsize_t;


int writeH5compressed(s1_t *data, hsize_t *dimsf, char *fichier, bool compressed);

#endif //PH5_FILE_UTILS_H//

